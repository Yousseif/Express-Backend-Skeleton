/**
 * Module dependencies.
*/

import app from './app';
import http from 'http';
import config from '../config';
var debug = require('debug')('Perseus:server');

/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT || config.port;
app.set('port', port);
/**
 * Create HTTP server.
 */
let server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
*/
server.listen(port);
server.on('error', onServerError);
server.on('listening', onServerListening);


/**
 * Event listener for HTTP server "error" event.
 */
function onServerError(error){
  if (error.syscall !== 'listen') {
    throw error;
  }
  const bind = (typeof port === 'string') ? `Pipe ${port}` : `Port ${port}`;
  // handle specific listen errors with friendly messages
  switch (error.code) {
  case 'EACCES':
    console.error(`${bind} requires elevated privileges`);
    process.exit(1);
    break;
  case 'EADDRINUSE':
    console.error(`${bind} is already in use`);
    process.exit(1);
    break;
  default:
    throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onServerListening(){
  const addr = server.address();
  const bind = (typeof addr === 'string') ? `pipe ${addr}` : `port ${addr.port}`;
  debug(`Listening on ${bind}`);
}
