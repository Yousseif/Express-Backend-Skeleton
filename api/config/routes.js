import { Router } from 'express';

export default () => {
  let router = Router();
  /* GET users listing. */
  router.get('/', function(req, res) {
    res.send(200, {order: 20000});
  });
  return router;
};
