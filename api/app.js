import express from 'express';
import logger from 'morgan';
import bodyParser from 'body-parser';
import routes from './config/routes';

let app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/users', routes());

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = {
    status: 404,
    error: 'Not Found'
  };
  res.status(404).send(err);
  next(err);
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send({
    status: 500,
    error: 'Internal Server Error',
    message: err.message
  });
});
export default app;
